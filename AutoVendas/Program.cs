﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AutoVendas.Entities;
using AutoVendas.Entities.Enum;

namespace AutoVendas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Qual o nome da concessionária? ");
            string nomeCon = Console.ReadLine();
            Console.WriteLine("Qual a marca dos veículos?");
            string nomeMarca = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Quantos modelos serão cadastrados?");
            int n1 = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Quantos funcionários serão cadastrados?");
            int n2 = int.Parse(Console.ReadLine());

            List<Modelos> modelos = new List<Modelos>();
            List<Vendedores> vendedores = new List<Vendedores>();

            Console.WriteLine();
            Console.WriteLine("Cadastro de modelos:");

            for (int i = 1; i <= n1; i++)
            {
                Console.WriteLine("Dados do modelo {0}", i);
                Console.Write("Nome do modelo: ");
                string name = Console.ReadLine();
                Console.Write("Quantidade em estoque: ");
                int quantidade = int.Parse(Console.ReadLine());
                Console.Write("Preço de Fábrica: ");
                double precoFabrica = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
                Console.Write("Preço de Venda: ");
                double precoVenda = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
                modelos.Add(new Modelos(i, name, quantidade, precoFabrica, precoVenda));
                Console.WriteLine();
            }

            Console.WriteLine("Cadastro de Funcionários: ");

            for (int i = 1; i <= n2; i++)
            {
                Console.WriteLine("Dados do funcionário {0}", i);
                Console.Write("Nome: ");
                string nome = Console.ReadLine();
                Console.Write("Nivel (Aprendiz/Efetivo/Lider): ");
                NivelDoVendedor nivel = Enum.Parse<NivelDoVendedor>(Console.ReadLine());
                Console.Write("Salario base: ");
                double salarioBase = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
                vendedores.Add(new Vendedores(i, nome, nivel, salarioBase));
                Console.WriteLine();
            }

            int quantidadeTotal = 0;
            double lucroTotal = 0.0;

            Console.WriteLine();
            bool reg = true;
            while (reg == true)
            {
                Console.WriteLine("Registro de venda mensal: ");
                Console.WriteLine();
                Console.WriteLine("ID do funcionário responsável: ");
                int idVendedor = int.Parse(Console.ReadLine());
                Console.WriteLine("ID do modelo vendido: ");
                int idModelo = int.Parse(Console.ReadLine());
                Console.WriteLine("Quantidade de unidades vendidas: ");
                int quantidade = int.Parse(Console.ReadLine());

                Modelos mod = new Modelos();
                foreach (Modelos modelo in modelos)
                {
                    if (modelo.Id == idModelo)
                    {
                        mod = modelo;
                    }
                }
                Vendas registroVenda = new Vendas(mod, quantidade);

                Vendedores vend = new Vendedores();
                foreach (Vendedores vendedor in vendedores)
                {
                    if (idVendedor == vendedor.Id)
                    {
                        vend = vendedor;
                    }
                }
                vend.AddVenda(registroVenda);

                quantidadeTotal += quantidade;
                mod.QuantidadeAposVenda(quantidade);
                lucroTotal += registroVenda.LucroTotal();
                mod.QuantidadeVendida(quantidade);

                
                Console.Write("Registrar outra venda? (s/n)");
                char validacao = char.Parse(Console.ReadLine());
                if (validacao == 'N' || validacao == 'n')
                {
                    reg = false;
                }
                else if (validacao == 'S' || validacao == 's')
                {
                    reg = true;
                }
            }

            Console.WriteLine("Relatório Mensal - Concessionaria {0} {1}: ", nomeMarca, nomeCon);
            Console.WriteLine("Vendas");

            foreach (Modelos modelo in modelos)
            {
                Console.WriteLine("Quantidade de unidades vendidas do modelo {0}: {1} unidades", modelo.Nome, modelo.QuantVendida);
                Console.WriteLine("Quantidade em estoque: {0}unidades", modelo.Quantidade);
            }

            Console.WriteLine();
            Console.WriteLine("Total de carros vendidos: {0} unidades.", quantidadeTotal);
            Console.WriteLine();

            Console.WriteLine("Salários:");
            Console.WriteLine();
            foreach (Vendedores vendedor in vendedores)
            {
                Console.WriteLine("Comissão total recebida pelo vendedor {0} {1}: R${2}", vendedor.Nivel, vendedor.Nome, vendedor.TotalComissao().ToString("F2", CultureInfo.InvariantCulture));
                Console.WriteLine("Salário total recebido pelo vendedor {0} {1}: R${2}", vendedor.Nivel, vendedor.Nome, vendedor.SalarioTotal().ToString("F2", CultureInfo.InvariantCulture));
            }

            Console.WriteLine("Renda: ");
            Console.WriteLine("Lucro total: {0}", lucroTotal.ToString("F2", CultureInfo.InvariantCulture));
        }
    }
}
