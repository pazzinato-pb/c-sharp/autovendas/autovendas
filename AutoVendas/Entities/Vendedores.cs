﻿using System.Collections.Generic;
using AutoVendas.Entities.Enum;

namespace AutoVendas.Entities
{
    class Vendedores
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public NivelDoVendedor Nivel { get; set; }
        public double SalarioBase { get; set; }
        public List<Vendas> Venda { get; set; } = new List<Vendas>();
        public Vendedores()
        {
        }
        public Vendedores(int id, string nome, NivelDoVendedor nivel, double salarioBase)
        {
            Id = id;
            Nome = nome;
            Nivel = nivel;
            SalarioBase = salarioBase;
        }

        public double PorcentagemComissao()
        {
            double multiplicadorComissao = 0.0;
            switch (Nivel)
            {
                case NivelDoVendedor.Aprendiz:
                    multiplicadorComissao = 0.015;
                    break;
                case NivelDoVendedor.Efetivo:
                    multiplicadorComissao = 0.05;
                    break;
                case NivelDoVendedor.Lider:
                    multiplicadorComissao = 0.1;
                    break;
            }

            return multiplicadorComissao;
        }

        public double TotalComissao()
        {
            double vendaTotal = 0.0;
            foreach (Vendas Venda in Venda)
            {
                vendaTotal += Venda.LucroTotal();
            }

            return (vendaTotal * PorcentagemComissao());
        }
        public double SalarioTotal()
        {
            double vendaTotal = 0.0;
            foreach (Vendas Venda in Venda)
            {
                vendaTotal += Venda.LucroTotal();
            }

            return (vendaTotal * PorcentagemComissao()) + SalarioBase;
        }

        public void AddVenda(Vendas venda)
        {
            Venda.Add(venda);
        }
    }
}
