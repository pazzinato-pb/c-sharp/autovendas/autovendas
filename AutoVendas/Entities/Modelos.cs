﻿namespace AutoVendas.Entities
{
    class Modelos
    {
        public int  Id { get; set; }
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public double PrecoFabrica { get; set; }
        public double PrecoVenda { get; set; }
        public int  QuantVendida { get; set; }
        public Modelos()
        {
        }

        public Modelos(int id, string nome, int quantidade, double precoFabrica, double precoVenda)
        {
            Id = id;
            Nome = nome;
            Quantidade = quantidade;
            PrecoFabrica = precoFabrica;
            PrecoVenda = precoVenda;
        }

        public double LucroPorUnidade()
        {
            return PrecoVenda - PrecoFabrica;
        }

        public void QuantidadeAposVenda(int quantidadeVendida)
        {
            Quantidade -= quantidadeVendida;
        }

        public int QuantidadeVendida(int quantidadeVendida)
        {
            return QuantVendida += quantidadeVendida;
        }
    }
}
