﻿namespace AutoVendas.Entities
{
    class Vendas
    {
        public Modelos Modelo { get; set; }
        public int Quantidade { get; set; }

        public Vendas()
        {
        }
        public Vendas(Modelos modelo, int quantidade)
        {
            Modelo = modelo;
            Quantidade = quantidade;
        }

        public double LucroTotal()
        {
            return Quantidade * Modelo.LucroPorUnidade();
        }
    }
}
