﻿namespace AutoVendas.Entities.Enum
{
    enum NivelDoVendedor : int
    {
        Aprendiz = 0,
        Efetivo = 1,
        Lider = 2,
    }

}
